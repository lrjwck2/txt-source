无能为力，只有放弃。做好了死的觉悟的大人们的話语，利安无法接受。
他今年十五岁了，即使是村外的魔物也狩猎过几匹。对於剑的本领，他还是比较有自信的。
因为如此，他对於其他的村民们的绝望的理由刻骨铭心地理解。作为那些魔兽们的对手，自己什麼也做不到。
但是，尽管如此他还是没有放弃。就这样什麼都不做的話，村民全部都会死，绝对。


利安现在居住的奇洛村正陷於绝望之中。
其理由有二。
现在，不知为何被称为魔兽的怪物群正集体朝这个村子赶来。
魔兽是魔物中是格外强烈可怕的野兽们。消灭它们，不能没有王国的骑士们的力量。
那群魔兽们，已经把邻村的格兰村毁灭了。那个村子的倖存者，有数名逃到了这个村子。
魔兽是吃人的。现在邻村的人被吃完了，赶来这边来只是时间的问题。
那麼，这个村的村民只有逃跑全部离开村子。大家都是这麼想的，打算去王国的城裡去避难，但是，这个地区的领主派遣士兵传达了一项命令。

『决不允许把魔兽引入王国的城市。禁止到王国的城市裡避难。』

也就是说，这是等同於领主的死刑宣告一样的消息。
你们要为争取时间而死的意思。在这个村子被袭击的时候，巩固城市的守备。
得到传令之後，村民们除了绝望之外什麼都没了。要麼就这样就等死，要麼逃去城市被当做反乱的罪人被杀。
逃是死，不逃也是死，还能如何？希望在哪裡？不论怎麼考虑生机，也只是被绝望压垮。
这就是村子的现状。面对悲叹的大人们，利安什麼也做不到的，什麼办法也想不出来。
但是，不想放弃啊。利安在这个村子裡出生，在这个村子裡长大。
这个村子裡有父亲，有母亲，还有年幼的妹妹。不想失去，不想失去他们啊。
就算是只有十五岁的利安，也多次体验过死的恐怖。和魔物战鬥，曾受过伤，曽流过血。
可怕。可怕。可怕。非常可怕。但是，不想什麼也不做等死。不想什麼也不做就这样让大家死。

所以，利安做了一个决定。
战鬥吧！即使只有我一个人也好，也要打给你看。
即使只是减少一只魔兽，也可能诞生出希望。即使只杀掉一只，未来说不定会改变。
会死的吧。自己一定会死的吧。但是，说不定其他的人有可能因此获救。
利安把决心藏在心中，单手持剑出了村子。
前往的方向，是邻村。会死的吧。生存是没有希望的。但是，我不入地狱谁入地狱。

「爸爸，妈妈，米娜，绝对不会让你们死的！
我，我，我会守护村子！绝对，不会让大家死啊！」

我嚎叫着。这不会减少我的恐惧，也不会有什麼奇迹的改变。
但是，这声喊叫堵住了我逃跑的想法。这样就好了，这样自己一定会努力到最後。战鬥到最後一刻。
做出了这种觉悟了的話，就好了。做完最後的决心，在继续赶往魔兽肆虐的村子的那个时候。
利安停下来赶往邻村的脚步。不，是不得不停下。因为那裡——有个变态。

「啊哈哈哈！多麼清澈美丽的空气！多麼炫目灿烂的天空！这就是我热恋数千年的人界吗？！
热血沸腾，真让人热血沸腾啊！现在我的全身充满喜悦啊！」

在大路的正中央，有个大笑的男人。看着这景象利安什麼話也说不出来，无语了。
为什麼，他是全裸的呢？首先这是第一个疑问。第二，为什麼，要抬头挺胸正大光明地站在路中央。利安什麼話都说不出。不能说出口。
总而言之，利安眼前有个无比强烈的变态，在道路中央大笑。
究竟他经歷了什麼。考虑到那裡，利安得出了一个答案。难道说，是邻近的倖存者？
据逃到自己的村子裡的倖存者所说，其他人都被杀了，或许，其实还有其他生还的人。如果是这样的話，他全裸的原因还稍微有点理由不足。
没准是在洗澡的时候，惊险地逃命出来的呢？那样的話就说通了，一定是这样的，不会错。请一定是这样啊！
我拼命地劝说自己（给自己洗脑），战战兢兢地对在眼前的变态询问出自己编织出的話语。

「啊，那个……那个，没事吧？」
「嗯？噢、噢噢噢噢噢噢噢噢哦哦哦！」


不要啊。没错，利安对非常讨厌的全裸男询问道。全裸的男人，终於發现了利安的身姿，發出了更兴♂奋的声音。

那个场景说实話，小孩子看到绝对会哭的。比就算在村裡比较高的利安也要高三个头左右，加上全身是锻炼後的没有浪费的坚固体格的美貌的青年，声音曲曲弯弯非常恶心。就算是利安也是战战兢兢，非常想哭。
不久之後抑制不住兴奋全裸的青年，抓住利安的两肩，全力地投来一句話。

「你！你就是『人』吗！？」
「……哈？」
「不是魔人、魔族、或者悪魔，而是『人』吗！？」
「是，是的！是这样！我是人！？」
「噢、噢噢噢噢噢噢噢噢哦哦哦！何等脆弱的生命！何等微弱的魔力！何等渺小的灵魂！

就是这个，这就是我的梦想中的人！我应该保护的生命！讚美我的存在！我的，只属於我的人！」

利安颤抖着摇着头，太糟了，为什麼我要向他搭話。
話说根本不知道问题的意思，再加上他兴奋的爆發让人心情糟透了。不明白问题的意义，为什麼是全裸的，你究竟是谁呢？这样的问题还没问出来就被他连环枪地搭話。
对於被摇来摇去的利安，不久之後兴奋稍微平静了吗，青年愉悦地微笑接着说。

「抱歉啊，人类。在数千年内，与你们见面一直是我的梦想啊。因此不知不觉，兴奋过头了。」
「啊，那个……没关係，不过，那个，您是哪位？」
「唔，失礼了。抱歉啊人类，吾就报上名号吧。很高兴你是这个世界上知道吾名的人类第一号。

吾名撒东！为了拯救你们人类，而来到这个世界的『利恩提的勇者』！

啊哈哈哈哈哈哈！为尊敬的吾辈欢呼吧！吾将成为你们的剑与盾，并成为你们绝对唯一的英雄！」
「啊……那个，撒东桑，这样称呼吧。撒东桑，『利恩提的勇者』是……」
「嗯，是你们人类们崇拜讚美传说的存在吧。那正是吾辈，撒东！」
（PS：男主的第一人称都变了）


对於意想不到的回答，利安哑口无言。

『利恩提的勇者』，那是人们都知道的童話故事，其内容是非常单纯的东西。

很久很久以前，魔物的王让这个世界在黑暗中包围着的黑暗时代，出现了一位来自异世界的勇者。
那勇者与十一个战友一起，被众神赐予传说的武器，在於魔物的王激战之後将其消灭，给这世界带来了光明。
之後，勇者们建立了新的国家，给予这个世界和平与安宁。就是这样的一个故事。
利安也在年幼的时候，从父母那裡多次听到这个故事。正所谓，面向兒童的童話，那就是『利恩提的勇者』。

年幼的孩子的話都玩过扮演勇者的遊戏吧。但是，随着长大就明白了那样的故事在现实中是不可能存在的。
但是，眼前的全裸的青年——撒东，毫不犹豫果断地认定自己是『利恩提的勇者』。
。
那个自信是从哪裡来的呢？平常时期的話，一定会详加询问，但现在是非常时期。
魔兽说不定马上就要攻进自己的村子裡了。突然想到这裡的利安，连忙呼吁撒东去避难。

「撒东桑！快点逃走！这裡很危险！」
「唔，危险是怎麼？让我觉得很危险的时候，到底是要回溯幾千年前的记忆？是啊，那是吾辈刚出生――」
「撒东桑过去的事怎麼都好！不久之後魔兽就会赶到这裡！不快一点会没命的！」
「魔兽啊？区区魔兽都会恐惧，嗯，越来越可爱了啊人类。真让我的内心不断回响你说的每一話啊。

然而那有什麼可怕的？是海王龙比内陆多吗？是陆狼鬼古维多路德吗？没什麼，对我来说都不过是宠物而已。」
「比，比内？虽然不知道说的是什麼，总之快逃吧！在这前面有我所居住的村子，快点吧！」
「嗯，你是我第一次见到的人类，虽然很想照你说的做，但为什麼你要往反方向走？

那个方向，不是有你所说的魔兽吗？不是有生命危险吗？」

对於撒东的提问，利安依然背对着他，握紧双拳组织语言。
那是刚才坚定的一点信念，那是刚才贯穿一切的勇气。

「我，我要去魔兽那裡，至少也要打倒一只。」
「嗯嗯，嗯嗯嗯……脆弱的人类啊，你刚才不是说有生命危险吗？你能打倒魔兽吗？」
「……打不过的吧。但是，即使如此我也要去。稍微一点点，说不定能争取点时间。说不明也能有赢的机会。」
「嗯啊啊啊……为何？为何要如此赌上自己的性命？是什麼在支撑着你？

打倒魔兽你能得到什麼？到底是什麼慾望让脆弱的你如此勇往直前？」

问道自己为何到做到如此地步，对於那个问题，利安忍不住露出笑容。
那种事情，不明摆着吗。这样渺小的自己做到如此地步的理由，只有一个。
把那句話说出口，是如此的简单。利安挺胸回答道，因为这是对他来说非常骄傲的一件事。

「――因为不想死。在村子裡，有我的父亲、母亲、年幼的妹妹、朋友，对我来说都是非常重要的人。
我一定会死的吧。可能我只是枉死。但是总比等死什麼也不做要强。
我，我是因为我的『慾望』才去的。这就是我的，渺小的我的，最後决定的道路。」

利安坦率的决心。为了那小小的慾望的，他决心赴死。为了自己，一切都是为了自己。
顿时两人间被静寂支配，利安慢慢地做了个深呼吸。去吧，在决心动摇之前，前进吧。他这麼下定决心。
接着正準备迈出赶往邻村的一步，但却动不了了，是因为他的肩膀被按住了。把视线转向那边，他的肩上，是撒东的手。
到底怎麼了呢？利安回头看向撒东，表情充满惊愕。


在那裡的是――异形。
在那裡的是――异神。
在那裡的是――异界。


看到眼前的景象，利安停止了呼吸。在那裡的，就是如此压倒性的存在。
和刚才为止的撒东比起来简直是别的什麼东西。虽然身姿样貌一样但利安清楚地认识到，这个存在——是怪物。
在狂乱庞大的『力量』面前，利安被威压得动不了。稍微不集中的話，就会昏过去的压力。
不久，撒东慢慢地开口。那是压抑着愤怒的鬼神。

「——无法容忍。你这存在、身姿、心灵，不能容忍」
「啊，啊，啊……」
「不顾及自己的性命，而去拯救家人？即使牺牲自己，也要守护爱着的人。你这傢伙，你你你你——」


会被杀。自己肯定会被眼前的怪物杀掉。
利安一下子就认定了那个事实，内心被彻底粉粹了。
即使面对魔兽，也没有动摇的决心，却对於眼前压倒性的存在没有任何意义。只是知道自己会死这一现实而已。
接着，等他开口了的时候，自己一定会死的吧。明白了这点的利安，除了向上天祈祷什麼也做不到。
神啊，如果神看到了这样的情景的話，拜托了。我的生命怎麼都好，请守护村裡的人们和家人。
然後，撒东对利安说出的临终的話语那是，无比地蕴含情感的——


「――你，不就像是『利恩提的勇者』一样吗！」


那声音，说是哽咽也不夸张。準确说，是哭喊啊。说的不是利安，当然指的是撒东。
利安，只能呆住了。为什麼没被杀？自己没死吗？
刚才为止的压迫感像玩笑般消失在空气中，眼前的是一副可怜的嚎啕大哭的青年。情况完全无法理解。
这样的撒东接下来说的話则让他陷入更大的混乱之中。

「不承认！决不承认！勇者，是我，撒东！怎麼能把它让给不知道从哪裡冒出来的你！
利恩提是我！你是……是啊，阿托斯！你的角色就当做阿托斯吧！嗯，这样好！你也这麼想的吧？」
「嗯，啊，嗯，那，是啊……？」


在适当的应答之後，利安开始慢慢地理解撒东所说的話。
难道这个男人，因为自己想要夺取勇者的角色，所以才生气的吧。
得到这个答案，不可能吧，利安感到混乱。但是，他的發言除此之外还能有什麼意思？结论正是如此。
顺便说一下，他说的是阿托斯指的是勇者利恩提的同伴中的一个英雄，神枪阿托斯。
总而言之，假如勇者扮演遊戏的話，撒东是利恩提，利安是阿托斯。这样的角色分配。
为什麼要现在，玩勇者遊戏。究竟为什麼会认为自己要夺走利恩提的角色？利安已经理解不了脑内已是残酷的状态。
但是，只有一点非常明了。撒东——他无比强悍。这个世界不能存在程度的强悍。
只有这点利安深信不疑。同时，心裡有了个打算。他的話，也许可以。如果是他的話，也许办得到。
撒东说不定能打赢魔兽。
想到这裡，利安就知道该怎麼做了。从撒东手下解放的利安，马上当场跪了下来，低下头，拼命地哀求道：

「撒东大人！求，求您了！请消灭魔兽！」
「哦，哦？怎麼了，突然……嘛，消灭魔兽！？」
「是的……如您所见，我和您相比太弱了，以魔兽为对方会手足无措，单方面被杀吧。

但是，您是不同的。和我不同，我感觉到了您无比寻常的强大力量。拜托了，请，请拯救村子吧！」
「那，那个是那个吗？勇者的，讨伐依赖？

为了拯救你们的生命，而去退治魔兽，这是勇者以外所不能达成伟业，请求勇者大人，你说的是这个吗！？」
「是，是的……很遗憾，我等就连拖延时间也做不到。但是，您的話……就能拯救村子！

求，求您了！能做到的事我什麼都会去做？！想要这条命的話随时可以献给您！所以请——救救村子」

当利安说到那裡的时候，他察觉到了异常。
不知为什麼，他从头顶上传来了呜呜声。说得更準确的話，擤鼻涕的声音。说得更像的話，是抽噎声。
究竟是什麼。当利安慢慢抬起头的时候，他已经说不話来了。
在他的视线的前方，撒东——哭了。而且非常严重，大哭，嚎啕大哭，鼻涕一脸。
对於这景象，利安顿时卧槽。一个大人，全裸，嚎啕大哭。你说这画面不卧槽吗？
正当利安犹豫着是否要搭話的时候，撒东边流泪边说道：

「是这个……就是这个……我追求的，理想乡……啊，这是多麼的幸福啊……
死都值了……世界末日都无所谓了……我是勇者啊，终於成为了勇者……呜咕……」
「啊，那个，那个……需，需要手帕吗……」
「不，不要紧……呜呜……拭去这喜悦的泪水，非常可惜啊……少年，名字叫什麼……」
「啊，事到如今……不，那个，利……利安。」
「利安，好名字。我灵魂的盟友，神枪利安！你的愿望，这勇者撒东接受了！

你那无比坦诚的愿望，勇者我会实现给你看！在这裡稍等片刻！」
「不，那个神枪利安，我没用过用枪什麼的……」


利安的話没能说到最後。
眼泪流了一脸，撒东保持着一副飒爽的表情当场起飞了。全裸的 全裸的 全裸的
然後在天上一边飞着，撒东一边短嘟哝什麼。那是咏唱，他擅长的魔人术式的一小节。
咏唱後瞬间，他的身体开始溢出的黑色光芒，像是被解放的野兽那样刺向周围的大地。
那是豪风。那是暴雨。那是箭击。锐利的光芒在地面不断地生长起来，然後他返回了地面。
之後看到的景象，利安一定终生难忘吧。撒东放出光芒的前方，是毁灭了邻村的魔兽临终的身影。
不只是一只。幾十头尸骸，被黑色的光之刃刺穿。不，不只是如此。
比那些魔兽更恐怖的魔兽和飞行龙，利安的十倍以上的体长巨大龙的身体倒在那裡。
撒东把断气了尸体的山，一下子扔到利安面前，面带笑容地说道：

「总之，把这附近能伤害到人类性命的魔兽们全部宰掉了，这样就够了吗？」
「啊，是，是的……」
「是啊，是吗？这样从你那裡收到的勇者的委托就完成了。啊，啊哈哈哈哈哈哈！

是啊！就是这个啊！为弱者而使用力量！这才是真正的勇者这种东西啊！我才是，我才是真正的勇者撒东！啊哈哈哈哈哈哈！」

一边听着响遍青空响的异形怪物的大笑，利安是这样想的。
也许自己是遇到了无比寻常的存在。
在那裡的是蓝天、放声傻笑的全裸男，呆然的少年，堆积成山的魔兽的尸体。

这就是自称勇者撒东和他称神枪利安两人的相遇。

