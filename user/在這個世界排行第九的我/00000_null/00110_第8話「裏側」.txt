王立哈伦斯魔法学园结束了今年的考试後的第二天。
如果说是不是风平浪静地一切结束了的话，事实上并非如此。
教师们全都在因为选定合格者、班级的编成而抱头苦恼着。

「今年的新生到底是怎麼了啊……」
「明明光是4名勇者加上四大贵族世家的千金大小姐就已经很吓人了」
「也就是修罗之年呐」
「我敢保证我S班绝对会是麻烦事一波未平一波又起」

众人异口同声地表达着感叹或是惊愕。
今年的确很特别，近年难得一见的天才全在今年聚集出现了。
不过在事前就已经得知了被认定为勇者的异世界人们，还有迪安诺斯家的千金的存在。
尽管对他们在考试中展现出来的力量很是惊讶，但那些也完全都还在预测範围内。
在他们心中最大的问题，是出在了别的人物身上。

「克雷斯・爱利西亚，他到底是什麼来头啊？」
「出生地是赫尔辛大陆西部，是从一个连名字都没有的乡下村庄过来的……」
「我觉得中间没有假。如果不是那样的话，他那等程度的实力，是不可能一直都默默无名的」
「嘛啊，不管怎麼说，他有着罕见才能这一点是事实呐」
「嗯。虽然他在考试中只用过冰属性魔法，但那个就足以匹敌魔法骑士了」

魔法被分为10个等次，最低为1阶，最高为10阶。
10阶魔法是难度最高的，倘若想要入读学园的话，那麼最低也必须得会第3阶的魔法才行。
然而，那位少年展示出来的魔法全都相当于第6阶以上的魔法。
看上去不过区区2倍，但两者之间却有如天地之隔。
6阶以上的层次是常人所无法抵达的，想要做到，需要努力以及拥有相应的才能才行。
做的事情虽然很单纯，但是这种练度完全不是一个仅仅15岁的少年所能做到的。

「而且那小子，感觉根本没动真格的，还留有餘力啊」
「艾萨克老师……」
「跟他打过的我最清楚不过了。克雷斯・爱利西亚，那小子是个怪物」
「连S级冒险者都这麼担保了啊」
「那都是以前的事了，那小子有那麼惊人的才能和见识，压根谈不上什麼担保，而是不得不认可啊」

之前当监考的是原S级冒险者艾萨克老师，他的实力是任何一个人都认可的。
而他却毫无支架之力地败北了。
他本人说自己根本就是耍着玩，完全敌不过对方。
最後对方还对他用了回復魔法。
两人之间有着如此之大的实力差，然而克雷斯・爱利西亚这一存在却毫无名声，真的非常不可思议。

「爱利西亚君跟勇者那边比，哪边要强呢？」
「那个不管怎麼说都是勇者要强吧」
「非也非也，我觉得实战上爱利西亚君要占上风」
「但是勇者那边可是异能持有者啊」
「凭我们的本事能打过他们吗……」

教师们会心怀不安也是不无道理的。
毕竟那是为了讨伐魔王而被召唤的4名勇者，大贵族家的千金，以及无名的天才。
除了他们幾人以外，还有不少个性很强的学生暂定在S班裡。
在身为最为优秀的班级的同时，唯独这一次还能够称之为异端班级吧。

「你们觉得今年的国际竞赛会变成什麼样？」
「呜ー嗯，帝国是个强敌，毕竟那边有战姬在呐」
「另外，我听说教国那边也选出了第20代剑圣了」
「总感觉变成个动乱之世了」
「是啊。感觉会發生战争，搞得人心惶惶的」

在旧的一年离去，新的一年来到来之前，国内会选出代表去参加国家与国家之间的亲善竞赛。
勇者、天才、战姬、剑圣，这些优秀的人物聚集一堂。
当他们碰撞在一起时，究竟会發生什麼事呢。
不过，就算引发了战争也只会是小规模的，会是国家级别的可能性很低。
毕竟近期之间，魔王们的动静活跃起来了，倒不如说人类之间开始準备联手合作了。

「有他们在的话，感觉魔王们也有办法对付啊」
「是啊。如果可以的话……希望把那个组织也解决掉啊」
「灾厄的数字（Namburs）吗……」
「那群人如果有那个意思的话，轻而易举地就能征服大陆，甚至世界————」
「这个话题就到此为止吧」

再说下去的话，论点就偏远了。
一句「题归正传」，为先前的对话打上了休止符。
现在应该讨论的是合格了的学生的班级编配。

「好了，我们来谈谈学生的事吧」

学园长起头说道。
春季已近在眼前。
崭新的学园生活也要开始了。
