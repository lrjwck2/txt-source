1
在傍晚的河边，我们正在推着自行车在走着。
唯和夏奈愉快地谈笑着，优人和路则埋头於有关最新的rpg话题中。
而我，筱崎奏士，则悠闲地跟在那样的四人身後。
远方回荡着日暮的声响，平静又略显憂伤。
我们这些傢伙都是住在附近，是从小就常常粘在一起的伙伴。唯是邻居家的孩子，比我年长一年，现在上高二。优人是街角香烟店兼杂货店的独子，和我同年。夏奈和陆是姐弟，住在稍远一些的公营住宅区，夏奈今年中三，陆今年中二
还在小学的时候大家经常这样一起放学，但是上了中学和高中分开以後，像这样聚在一起的机会也变少了。
今天只是偶然地大家的时间都对上了，又偶然在车站前碰面。好久没有像这样一起回家了。


“但是奏士那麼早回家还真少见呢。”


最年少的陆回头看着我。看来遊戏的话题已经告以断落了。


“嗯。啊，今天剑道场要定期清扫，所以没有社团活动呢”


“是呢是呢，这傢伙要不是遇上这种事儿就一直在挥竹刀呢，偶尔的休息也是必要的哦”


优人露出调皮的笑脸来嘲弄我。


“奏士从中学就开始练习剑道吧？很强么？”


“啊啦，陆你不知道么？在中学时期最後的大会上，奏士拿到了县大会第四哟”


“诶？”


“是呢是呢。出现了美少女剑士什麼的，一时间还成为县内各中学的话题呐，奏士”


优人熟门熟路地搭上了我的肩膀。
遗憾的是我的个子很矮，和体格娇小的夏奈有的一拼，相比之下唯还更高一些。而且我肤色白皙线条细腻，以前留着长髮的时候看上去毫无疑问是个很漂亮的女孩。
我讨厌自己这样的外貌，为了锻炼自己更进一步的沉溺于剑道之中，头髮也特地剪短了。


“优人，别说这种话了”


我推开优人的手。就算是已经剪短头髮的现在，当缠上手巾的时候也常常会被错认成女孩子。
真是的，讨厌呐.....


“真是浪费啊。奏士若然加入研究部的话，姐姐我会让你变得更加可爱的呢”


唯的手捂着脸歪着脖子。虽然看上去是稳重的大姐姐，可是这个人会把我当做玩具玩呐。


“奏士，再把我的衣服借给你吧？”


神采奕奕的夏奈笑着问道。


“闭嘴，然後请你去死”


我冷冷地回答道。夏奈不满地鼓起脸颊。


“太过分了~奏士你太过分了~还有，那个实在是太合适了！”


因为夏奈的反应让大家都哈哈地笑了起来。
我虽然很努力地尝试忍住，不过最後还是跟着一起笑了。
真是久违的快乐傍晚。我觉得偶尔这样也不错。
说说最近那些發生在学校的事，学习，朋友，遊戏，甚至是新鲜的恋爱故事什麼的。和这些傢伙在一起真是有说不尽的话题。
但是在路程途中，夏奈最先發现了异变的出现。
她突然跑了起来，灵巧的翻下河堤，接着一动不动地凝视着河面。


“怎麼了？夏奈”


在那之後优人和陆也陆续下到河滩。


“嗯？那是什麼啊？在發光么？”


“恩，啊啊真的啊，沉着什麼东西吧”


“夏奈经常找到些不三不四的东西啊”


陆开玩笑的说道，夏奈则还以颜色。


“喂，危险，会掉到河裡的”


虽然我在岸堤上大声呼喊。但是那些傢伙已经得意忘形了，他们都在幹什麼阿？虽然现在是夏季，但是掉到河裡然後变得浑身湿漉漉地回家，是会伤风的吧。
突然，河滩上刮起一阵强风。
不知不觉间我闭上了眼睛。


“啊，呀！”


然後突然传来了夏奈的悲鸣声。
我睁开眼睛的瞬间使我感到了愕然。
眼前的光景令我难以置信。
河面好似沸腾一般，流水声响彻云霄。
一个水柱向着高处不断延伸，就好像大蛇从河面开始抬头一般。
谁都不禁失声起来。
高高扬起的水柱一瞬间停住了，紧接着就好像有意识一样向着夏奈她们那边崩落了。


“夏奈！优人！陆！快逃！”


、我一边喊着一边扔下自行车，向着河堤下跑去。
但是水柱更快。
在水柱落下的方向，三个人的身影消失了。
落到地面上的水好像有意识一般的再次收束为水柱，这次是向着附近的我和唯袭来。


“唯姐快逃！”


我这麼叫的瞬间，压倒性的水量将我吞没。
水的寒冷以及那压倒性的质量所带来的压力将我的意识打飞了。
这只是瞬间的异常事态。
接着就什麼都不知道了，我的意识中断在了那裡。
在小时候，我率直地憧憬着强大，憧憬着在祖父道场所见到的日本刀那样笔直的力量。
我最喜欢“奏士，奏士”的叫着我，疼爱着我的祖父了。原本是警察的祖父在退休後经营着教居合道和剑道的道场。喜欢小孩子的祖父也非常疼爱我，但在道场的时候就不一样了。
带着任何人都比不上的锐利目光，用钢铁一般的手臂挥舞着日本刀。在道场中总是充满了可怕的集中力和气魄。
当时还是孩子的我虽然从心裡恐惧着那个姿态的，却又十分向往那个姿态。就是从那时开始，因为憧憬着祖父，我也想锻炼出那种刀刃一般的氛围。
为什麼现在会想起了这些事呢。
不明白。
但是，总觉得自己现在，必须想起这件事。
接着我慢慢地睁开了眼睛。
最初映入眼帘的是深邃的森林。青草浓烈的香味钻入了我的鼻子裡，草堆为我的脸带来刺痛感，在模糊的意识中我渐渐开始清醒起来。
我慢慢地起身。
虽然没有什麼特别疼痛的地方但四肢还残留着疲惫的感觉。
深呼吸~
我顺手撩起扑簌簌地滑落的长髮。
这个动作做起来非常自然，但很快我就感觉到了惊人的违和感。
现在我应该是运动型短发，应该不存在会滑落的长头髮才对。
我摸了摸自己的头顶
有头髮，而且还很长。
发尾披到了肩胛骨附近，浏海则是贴到眉毛的程度。
这算什麼？
是优人和夏奈的恶作剧么？
我慢慢地试着拉一拉动头髮。
....好疼。
这是理所当然的吧，拉扯头髮就一定会疼。
我战战兢兢地把头髮放入视野中，
如果是黑髮的话那还留有现实的味道吧，日本人的头髮就是黑色的。但是进入我视线的我的头髮是通透闪亮的银色。
银.....发？
这都是什麼乱七八糟的情况？
头有点疼了
我大大地深呼吸
因为没有镜子，关於头髮的事暂且保留。
首先应该把握现状
我站了起来，环视四周。
我所处的位置是森林，不管哪裡都生长着郁郁葱葱的树木。
确实我和唯，还有优人他们是被河冲走的，可是在河滩的四周并不存在着森林。
我究竟被冲到哪裡了呢？优人他们，还有唯怎麼样了啊
我不抱希望地四处顾盼，，轻轻摇曳的长髮让我陷入更大的忧郁之中。
突然间一个白色的物体进入我的视线裡。
我下意识地跑向白色物体那边，是优人的一只运动鞋掉在地上。
我慌忙四处寻找优人的踪影
啊
在不远处我发现有具体格良好的躯体倒卧在了茂密的草丛之中。那是，优人！


“喂，优人”


我大声喊道，然而我的声音似乎有些违和！ ？
......十分微妙地有些高呢。
我觉得应该...是喉咙悲剧了吧，总之现在应该帮优人为優先。


“喂，优人，起床了”


总之好像没有外伤。
我轻力拍打他的脸颊，优人呜呜呜地呻吟着爬了起来。


“那个，究竟發生了什麼？”


“我不知道。总之现在找不到唯她们，可能他们已经不在这一带了。”


“呃，啊，是这样啊....”


看来意识还没有完全清醒，优人摇着头看向这边，紧接着惊讶得整个人硬直了。


“.....是奏士，么.....?”


优人一脸狐疑。


“你在说什麼呢，那是当然的咯。头髮总觉得有点奇怪，究竟是怎麼回事？感觉像是睡着的时候被恶作剧了什麼的....不是你干的吧？”


“说什麼白痴话呢。你到底發生了什麼....看起来简直是个超级美人啊”


对於发着呆的优人，所开口脱出的傻话。
我立刻敲打了他的脑袋。


“笨蛋，那个neta已经够了。不要一直發呆了，快点一起找大家！”


“不，所以说那个声音也是....以前总觉得你像是女孩子，你果然.....那个样子怎麼看都是美少女.....”


问答无用，我再一次敲了优人的头。


“奏士，那个胸，是塞了什麼吗？”


对於被敲头後却完全没反应的优人指着我的胸口提出问题。
我立刻低头看去，确实制服的胸口鼓了起来。真的不知道是谁在什麼时候搞的这种精緻的恶作剧.....
我愤怒地偷看自己的胸口，接着轮到我硬直了。
.....不可能！
.....绝对不可能！ ！ ！
我脑袋裡一片空白。因为很羞耻，我赶紧把领口周围復原了
我变成女的
真的.....
是梦么？
不，大概是真的吧。
但是，这都只是袭向我们的一系列异变的开始。对此，现在谁都还不知道将会發生令我们意想不到的大事。
