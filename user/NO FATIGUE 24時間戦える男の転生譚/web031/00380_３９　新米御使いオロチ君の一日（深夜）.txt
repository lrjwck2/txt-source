深夜――

我在确认孩子们都已经睡着之後，悄悄地溜出了房间。
作为预防措施，我让麦露薇睡在房间裡以防万一巡视的人会前来。

孩子们的房间在乌鸦巢（nest）的後方，所以没有轨车的話，人们是无法进出的。
关於这点的話，我听说这是为了更好地保护後面的孩子，但是就我而言，我只会觉得那样是为了裡面的孩子逃跑而已。

乘轨车的話，要是不能通过到处都有的传声筒以一定的节奏发送信号的話，导轨的方向就不会切换好，也就无法抵达目的地。
更何况轨车没有两个人互相拉动导轨就没法运行，所以一个人根本没法让它动起来。

当然，对我而言，我可以用【物理魔法】让它强行移动。
但是，如果我要让轨车在夜裡移动的話，那就会造成声响，所以我也不是可以随心所欲地使用它。

那麼，你可能会想，我只要在导轨上走就好了，但是那正是建成不让人这麼做的样子。（Ant：你可以飞啊。）
隧道裡的某些地方，挖出了很多很深的坑洞，只有经过横跨在上面狭窄的轨道桥才能通过。
有些身手灵活的御史可能办得到，但预期到了那种情况，在轨道桥上就布置了很多拍板。
因为设置了这些拍板，所以哪怕只施加上一点重量，都会發出响声。如果你要是什麼都不想就穿越过去，那你就会有大麻烦了。
当我初来乍到的时候，我也触发了这些装置，於是我只好依附在天花板（隧道顶）的孔洞内，等待前来查看状况的御史离开。
有幸的是，这些拍板显然常常会在轻微的震动下误报，所以过来的御史没多做调查便离开了。

总之，像这种防止内部成员私自离开巢穴的措施在第二层和三层随处可见。

我的前世，邪教团体也会把他们的信徒隔离在偏远的宗教设施中，防止其与外界的人接触。
因为持有从外界获得的信息，他们可能就会认识到他们的宗教组织的客观评价，而对他们的洗脑有时候就会被破坏掉……这就是我认为的理由。

很可能这也是《八咫鸟》像这样把他们的巢穴隐藏在这麼偏僻的地方的理由。

我蹑手蹑脚走进轨车间，借着【黑暗视觉】确认没有被别人看到，然後踩在铺倒在地上的轨道上走进隧道的内部。
在一片黑暗中，我行进了大约百米。
在一处天然洞穴的开口处，我离开了轨道，然後钻进一个只有1米高（深）的洞中。

洞裡的空间比外面看见的宽阔。
就算这只是对我这个身高不到1米的人而言的。如果是一个正常的成年人进来的話，就算弯下腰，也会磕到头。

从洞裡，可以进入到另外3个洞穴中。
在那三个洞的背後，延伸出1米高，大约80厘米宽的矩形通道。

——这是一条我闲时使用【地（土）属性魔法】挖掘出的秘密通道。

我把这通道成为“管道（Duct）”。
那与通风井不同，它只是在附近的环境（岩层）中通过而已。（Ant：应该是指并非通向外界的意思。）

在早上的挖掘期间，我也稍微耍了点小聪明。
当我使用【地属性魔法】的时候，我会在地下制造出空腔，来为我挖掘“管道”的时候提供标志。
然後，当我在夜裡快挖到它附近的时候，我就把耳朵靠在管道的岩壁上，使用【倾听】利用声音（敲击）的回声来确定这些空腔的位置，然後挖出一条通向那裡的隧道。
这样就有些麻烦，因为要是我不小心打碎了地表或者岩壁，就算我用魔法修补好，那也会留下明显的痕迹，所以那也没办法了。

“呃……我应该在昨天就把4层给测绘好了。”

今天是第5层——我的目的是调查挖掘现场和这个宗教组织的最高层的私人房间的位置。

然而，当然，赞恩也是在最高层所在的那一层的。
ガゼインは達人級スキル【気配察知】を持っている。
因此，即便我潜伏在地下，也有可能会被侦测到。
鉴于此，直到我【気配察知】的等级超过赞恩之前，我都不能着手调查最高层所在的地层。

当我朝着向下倾斜的隧道进发时，戴在我脖子上的“忠诚”的项圈發出了声音。

“吓……我得更换数字了。”

这项圈很像我前世配在自行车上的车锁。
那是由用不知名金属造出的坚固的锁环和一个数码式的挂锁组成的。
密码由5位数字组成。
把这玩意套在我脖子上的御史。

“你可以自由地尝试把它取掉，知道吗？但是，你要是把上面的数字弄乱了，那就会有足以让你昏厥过去的疼痛。”

如是说。
但是当我对它使用【鉴定】的时候：

《附属之环：当说出登记过的关键字时，附属之环将定下契约。当前的关键字时“忏悔”。当附属者意图强行摘下附属之环时，它将吸收（附属者）20MP。》

说这是“忠诚”真是天大的谎言，它其实就是一个奴隷项圈。

但是，它的效果却出乎意料之弱。
简言之，如果低于20MP的人打算强行（将自己）释放出来，并失败的話，那他的MP将会被耗尽然後昏过去而已。

不……这其实取决於你怎麼看，它说到底还是很神奇的东西。
因为即便是对於一个拥有能和茱莉娅妈妈相比的MP量的人，他们每次也只能尝试10组数字。
要是每天只有10次机会的話，在1万种可能性中筛选出正确的数字（密码）来也如同大海捞针。

話虽如此——你可能已经知道了。
这对我而言不过是玩具罢了。

我在一周内就把它解决了。

它刚刚發出声音其实是因为我把数字放在了解锁的密码上，所以这项圈差点因为我走路时的震动而松开了。
沉没在沥青一般的黑暗中，我根據指尖触摸的感觉改掉了数字，确保我脖子上的项圈處於锁定的状态。

顺便，我的项圈的密码是91122。
我猜这第一个数字应该不会是小数字，於是我按照7、8、9的顺序依次尝试，所以我比预期的更早选出了正确排列。

此外，在他们睡着的期间，我一一检查了其它孩子们的项链，结果出乎意料地发现他们的解锁密码也全是91122。
像这样重複使用密码，如果被总务科的小池先生听到的話，他估计要发疯的吧。

好吧，要是他们把密码管理表就那样白纸黑字地丢在那裡，那在某些情况下被谁偷偷看了一眼也是有可能的吧。
要是那是一串不可能找到的数字的話，那我也不是看不出密码只保存在首领手裡的好处。

这样（统一管理密码），当出现了某些状况的話，他们就能取下孩子们的项圈。

这麼说来，孩子们其实还被套上了一个相比之下还要麻烦的“项圈”。

——洗脑。

就算我取下他们的项圈，那些从罪恶审问开始，长久以来就一直接受着专注于洗脑的宗教教育的，从父母身边被带走的孩子们也不会想要从宗教组织中脱离出去。
反而，他们有可能还会把我的事情上报到宗教组织那一方去。

所以我要在去下他们可视的项圈之前，先破坏掉那个看不见的“项圈”。

我要拯救这裡的孩子们。
为此，我才挖掘了这些管道，将整个巢穴的地图绘制出来，并调查项圈的解锁密码；而与此同时，我在白天勤奋地劳动来赚取他们的信任，同时暗地裏增加我在宗教组织中的情报来源。

夜裡，我给孩子们讲故事不仅是为了过得愉快。
要解除洗脑，首先要让他们信任我。
因此，我得和他们一起遊戏（娱乐），让他们认为我是他们的战友（同胞）。

同时，通过这些故事，我也想着要透视这宗教组织的教义。
这个宗教组织的价值观过於一边倒。
因而，他们得以体验到另一套价值体系，那样他们肯定会萌发出违和感。
然後如果这种违和感一点一点堆积起来的話，那些孩子或许就能用自己的力量克服宗教组织在他们身上施加的诅咒了。

要达到这样的结果需要大量的耐心，要是我只是毫不客气地否定这邪教组织的教义的話，我能得到的只会是反弹罢了。
我想着充分利用【不易不劳】，坚持不懈地保持努力。

——話说完了，深夜就是提升技能等级的时间了。

首先，我直接（把面板）展示出来最快了。
当！

《埃德加·徹贝尔（徹贝尔子爵的四子|圣塔玛那王国的贵族|《绯红之子》|《无限大蛇》）

LV.32
HP：67/67
MP：3178/3178（584↑）

技能：
·神話级

【不易不劳】-

【即时理解】-

·传奇级

【意念力】1（NEW！）

【精神魔法】2【NEW！】
【鉴定】9（最高）

【数据库】－

【心念交流】3（2↑）

·大师级

【投掷技巧】2（NEW！）

【手裡剑技】2（NEW！）

【物理魔法】9（1↑，最高）

【火元素魔法】1（NEW！）

【地元素魔法】4（NEW！）

【附魔】3

【魔力控制】7（1↑）

【无咏唱啟动】8（2↑）

【魔力探查】1（NEW！）

【魔法语言】3（2↑）

【感知存在】4（NEW！）

【黑暗视觉】2（NEW！）

【雕刻】3（NEW！）

·普通级

【矛投掷技】5

【飞剑技】5（3↑）

【手裡剑】9（3↑，最高）

【斧投掷技】2

【掷刀】5（3↑）

【钢丝技能】4（NEW！）

【暗杀技能】5（NEW！）

【跳跃】4（NEW！）

【火魔法】9（1↑，最高）

【水魔法】4（2↑）

【风魔法】7（1↑）

【土魔法】9（7↑，最高）

【光魔法】8（3↑）

【雷电魔法】7（NEW！）

【念动力魔法】9（最高）

【魔力操作】9（最高）

【多重啟动】9（最高）

【魔力感知】9（6↑，最高）

【暗号解读】2

【倾听】9（2↑，最高）

【远视】4（2↑）（Ant：这是病，得治）

【夜视】9（2↑，最高）

【暗步】7（NEW！）

【雕刻木材】9（最高）

【烹饪】2（NEW！）

<善神的祝福+1>》（Ant：天杀的状态）

首先，先看一下我的别名吧。

《无限大蛇》。

我获得这样的别名大概就意味着我在众多御使之中享有一定的美誉吧。
我得小心不让人怀疑到任何有关我的【不易不劳】的线索。
但是，出乎意料的是，想要在不累的时候装出很累的样子还真的很难，所以他们或许会以为我拥有无限的体力吧。
我的别名大概取出於此。

然後，在技能的上方，这次有很多的内容。
要是你觉得太碍事的話，看一眼就过也没什麼大问题。

我们从【感知存在】说起吧。
这是我的【倾听】技能达到满级所获得的（奖励）。

【倾听】的目标是声音，但是【感知存在】就不止於声音了，它可以察觉到人或者动物散發出的微弱的存在感。
随着技能的等级上升，它的範围和精确度都会提升，而目前我能揪出和我位於同一座建筑物内的他人的移动。
如果我只想知道是否有人在的話，我大概能在接近百米的範围内察觉到。
有这个技能，我就不太可能会像在霍诺城裡那样沦为他人奇袭的猎物了。

接下来，【雕刻木材】和【雕刻】。
在字面上那样嗜血成性的宗教组织的每天生活中，御史们会被敦促进行木工来放鬆。
因为这样不会花费任何金钱，同时会让他们在运用刀器上更加得心应手。
在巢穴中主要分成业余木工圈和艺术圈。而我就是属於木工圈的。

我对於我们每天只能雕刻恶神的雕像很不满，但是木雕这件事本身却是很有意思的，於是不自觉地，我就热爱上了这门艺术。

此外，木雕还有一个依附于其上的技能。
“技能”就是将人自己的技巧和神明词赐予的礼物（天赋）结合在一起的产物，所以到底它提升得比我前世的【雕刻木材】和【雕刻】要快得多。
最终，我可以在20-30分钟之内完成创造出一尊平板电脑大小的恶神雕像。

木雕非常有趣，我一不小心就沉迷其中了。
因为我拥有【不易不劳】而不会厌倦，一旦我“沉迷”其中的話，就无法自拔废寝忘食了，我可以连续半天，乃至整整一天都做木雕。
又一次，我想着在提升技能之前稍微玩一会木雕，结果当我回过神来的时候就已经是白天了。
这也可以说是【不易不劳】的反面吧。

但是多亏了这样，我的技能等级一路高歌猛进，於是我便获得了大师级的技能【雕刻】。
【雕刻木材】和【雕刻】的区别在於它不仅可以应用在木头上，还可以雕刻石头或者金属。
置於修理一些从废墟拿回来的垃圾的时候，这些技能迟早会派上点用场的。

我得到了意料之外的技能，所以我自己还是很开心的。

有很多技能来自于我在宗教组织中参加的暗杀训练。
【投掷技巧】是【手裡剑】达到满级时获得的满级奖励，是与我的【手裡剑技】同时获得的。
那是从梭裸祭祀那裡得知的技能。
它拥有能够本能地知道投掷出的东西的轨道和打击点这一简单而实用的功效。

除此之外，【钢丝技】是我和赞恩训练时用的。
【暗步】、【跳跃】和【暗杀技巧】是我从指导组那裡学来的。
像【烹饪】这个奇怪的技能是从烧制日式烧饼的时候得到的。
我觉得技能应该食指和战鬥有关的，但我猜类似这样的技能应该也是存在的。

同时，我也终於将【火魔法】的等级练到了最高，并和茱莉娅妈妈一样获得了【火元素魔法】。
虽然看上去我不像妈妈那样拥有很高的资质，但是作为和她流着相同的血脉的人，我肯定也要获得这些技能的。

此外，满级了【物理魔法】的我获得了伝説級【意念力】。
这个技能直接通过意念力释放魔力，所以相比【物理魔法】需要依靠魔法将魔力转化为念动力，这个技能的能源（魔力）利用率更高，得以更强的输出，并且可以更直观地进行操作。
如果你想要强制增加魔法的输出力，那它的MP消耗就会一再地升高，所以在想要增加MP上限而尽快将魔力消耗掉的时候，这个技能应该就能展现出其作用了。

另一个与魔法有关的就是，我通过满级【魔力感知】获得了【魔力探查】。
看来训练获得麦露薇的【心念交流】和【精神魔法】也有获得【魔力感知】的次要效果。
你可以把【魔力探查】当做是【魔力感知】更加高层次的一种表现形式，但是其中一个简单但是重要的差别就是我可以将探查到的魔力的量转化为MP值了。

我也能说我是为了一些简单的事情而获得了【精霊魔法】，但是要等到我能使用它来战鬥的話，可能就要到很久以後了。
到时候我就会告诉你们关於它的一些有趣用法。

然後最後，星空演奏者【雷电魔法】。
要获得这个技能真是费大劲了。
诶？你问我不是有这方面的天赋么？
确实，就像梭洛祭祀说的那样，我可能拥有这方面的天赋。
但是我并不知道【雷电魔法】的魔法符号。

【无咏唱啟动】不能在不知道其魔法符号的情况下使用。

【雷电魔法】是新的魔法，所以在《阿巴顿魔法全书》中也找不到，麦露薇对此也不知道。

麦露薇通过请求风与水的精灵给我上演了一场产生闪电的把戏（魔术），但是那并不是我索要寻找的东西。
换言之，我连最基本的引导都没有。

那我最後是怎麼得到的呢？

我的方法是这样的。
首先我在使用别的种类的魔法的时候，想象闪电的样子。
然後，我尝试写下魔法符号。

当然，我是不知道【雷电魔法】的魔法符号的，但是按照一般的规律，魔法文字从左写到右，由上写到下。
利用这条规则，我用手指从左上角沿着随机的路径画到右下角。
与此同时，我使用【魔力感知】和【魔力探查】感觉魔法符号种魔力的流动。
用那种方法，虽然绝大多数时候我什麼都感觉不到，但是很小概率地，会有魔力在中途流出身体。
那样的話，就说明我画到了与【雷电魔法】的魔法符号相同的路径上了。
用这种理论，在反复尝试中，【雷电魔法】的魔法文字从左上角到右下角的形态就渐趋明朗了。

——没错，这是完美的理论。

那就是我在着手这麼做的时候所想的办法，但是事情并没有说的那麼简单。
将整个魔法符号描绘出来似乎是需要进行咏唱的，所以即便魔力流了出来，你也不知道在这条随机路径上的那个部分是正确的。
最後，我写出了和它非常相像的“文字”，确认写出文字时的感觉，对此的比较和验证就是（实际使用）“一发”，那就变成了单纯的重复工作了。
然後，经过10天每天深夜3小时的工作，我终於“发现”了闪电的魔法文字。
ξ（雷电）。

首先，那得有一个难以被发现的形状。
但是，经歷这麼多麻烦得到的成果都是值得的。
而到我真的使用出来的时候，我就知道，在我至今为止使用过的所有魔法当中，这个是最适合我的。
我对着魔力的流动知晓得清清楚楚。

茱莉娅妈妈很可能在她使用【火魔法】的时候也是这样的感觉。
不用说，照我的做法来的話，她肯定会不耐烦的。
我终於得到了，所以我决定使用【雷电魔法】来加深我和孩子们的羁绊。
我借来一个从废墟中挖来的金属球，然後对它施加了微弱的【雷电魔法】。
然後，要是我触碰这个金属球的話，就会“噢，好神奇，我的头髮到处乱飘呢。”
这只是剽窃了一个非常简单的科学实验罢了，但是这在孩子们当中相当受欢迎。

梭洛祭祀说过这个字（世界，应该是英翻少打了个字母）的人们还不能将闪电想象得惟妙惟肖，但孩子们呢？
如果他们看起来是能够保守秘密的話，我就不妨也让他们一试。

这麼想着，一边训练着我的【黑暗视觉】技能，我一边朝着隧道的深处前进。
这个【黑暗视觉】是【夜目】满级的奖励。
【夜视】在根本没有光线的地方是没有用的，但是有【黑暗视觉】的話，虽然我不知道到底是根據什麼逻辑，即便在完全无光的隧道中，我也能多多少少看见我周围的环境。

今天我的目标是第5层，所以我在管道的中途开始使用【地属性魔法】。
使用【感知存在】和【倾听】来探寻我目前的位置需要成吨的专注力，但是对我这个拥有【不易不劳】的人来说并非难事。

花了两个小时，我差不多调查了第5层的三分之的面积。
主要在废墟的周围。
因为要是不小心走进我根本不知情况的最高层所在的区域，很可能就会惹出大麻烦。

——好了，接下来，就要赶紧赶回我为了升级技能制造的看空间裡进行反复作业了。

而正当我这麼想的时候，我接到了来自麦露薇的【心念交流】。

