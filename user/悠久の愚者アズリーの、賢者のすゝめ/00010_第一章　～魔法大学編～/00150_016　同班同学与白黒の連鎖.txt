――　四月一日　上午九点　魔法大学中央校舍　――

我、波奇还有莉娜，来到了中央校舍中庭、用来锻炼和召开集会用的魔育馆。
在那裡能看到五十人左右的新生、和数百人的在校生与魔法师的身影。

入学式开始前，我正準备在新生席坐下，德拉甘和艾琳就过来搭話了。虽然只是没什麼营养的个人祝词，也烦人地招来了周围的视线。
顺带一提德拉甘是作为来宾来学校的。

这裡的监护人席也设置了不少，波奇也坐在其中，一身毛茸茸的十分惹眼。那傢伙也成为了注目的中心。
虽然允许使魔作为同伴入场，但还是不能就座新生席，只能老实地呆在监护人席。不过它好像在和周围的淑女们聊天的样子……应该没大问题吧？

由於十五岁以上才能成为新生，於是分为了女子席和男子席。以前明明是一起坐的，这样的改变总让我感到很违和。

然後入学式开始了。
齐唱校歌

『即便失去双腕　仍要前进

　即便失去双足　仍要前进
　祖先之心　炽热之魂
　魔法之心　希望之光
　白黑连锁的终焉尽头
　贝拉涅亚　贝拉涅亚
　啊啊 我们的　魔法大学　魔法大学』

唱出来总感觉哪裡不对劲。
接着是魔法大学校长、也是《流星的魔战士特加仑（Tengallon）》的致辞。
特加仑乃是以火、土、风的复合魔法著称、战士出身的魔法师。严格、并且很难说話，对他表露出温和的样子应该就行了吧。
头顶白髮稀稀疏疏，皱纹有些显眼。摸不清他的眼到底有没有睁开，让人感觉根本看不见东西，还真是和魔法大学的校长相衬的风格。
在来宾德拉甘祝辞完、忒蕾丝介绍完教师之後，终於该在校生发表欢迎贺词了。
学生自治会会长《沃伦（warren）》。无论是德拉甘还是特伦加，在致辞时一直人声嘈杂，但沃伦一登场，底下立马鸦雀无声。
恐怕，这受一般公开的友谊赛的影响很大。他拥有被称为《黑帝》的实力、还有传言说他是下届的六法士之首。
不止身处黑的派系，还身穿黑色法衣、身披黑色斗篷，连头髮也是长长的黑髮，肯定肚子裡也全是全黑的吧。（意指腹黑）

虽然是盐系男长相，五官齐整，但目光十分锐利。（注※盐系男：指那些具有清淡感觉的男生。）

是错觉吗他好像正在看我这边的样子？嘛是错觉吧。

接着是新生代表致辞。
不，我不是代表啦。而且莉娜也不是，我们两人都谢绝了。
虽然是首席，但通常考试满分的也有三个人。那麼就去拜托那三人吧、我想全推给别人的内心立马就被波奇看穿了。
然而，莉娜也是同样的想法。莉娜虽然有自信致辞，但是并没有那麼高的涵养，只是可以读书写字这样的水平。
从出生起就一直呆在法鲁镇，这也是没办法的。镇子安定下来後，莱昂和蕾娜也有教我很多事情，但果然还是比不上这个学校的新生们。
当然，只要是关於魔法的事莉娜比别人都要加倍努力，也比他人优秀。要是这个学校能让莉娜更加成长就好了、我这样希望。

新入生代表是和我、莉娜同级的《奥尔路（Ornel）》，是个苍发蓝眼、戴着眼镜的优秀男生。
发言干脆利落，热情洋溢的表情满溢出他的自信。

入学式结束了，我们在忒蕾丝的带领下来到了最初的教室。
刚入室时，波奇也一起进来了，接下来一起来上学应该也没问题了吧。
教室中每张桌子上都放着木头雕刻的姓名牌，从桌子上的板状配件来看应该是滑入式的。

“那麼，确认完自己的座位後，把姓名牌插入板内坐下。”

“master，连我的姓名牌都有哦！”

“好了快闭嘴。”

和使魔在一起，怎样都很显眼。周围既投来了兴趣为重的视线，也有嗤嗤偷笑的声音混杂着。不用说，那笑声裡肯定也有莉娜的声音吧。
教室呈阶梯式，非常宽阔，感觉是开展百人讲座的地方。怪不得波奇也能坐下啊。
我和波奇坐在中央，莉娜在最前列的最右边，坐在靠近走廊的地方。
和弟子在这样的地方一同坐着，也别有一番风味。

忒蕾丝重新寒暄了次，将明天开始的校园生活简单的说明了下，然後给每个人配发了两枚卷着的羊皮纸。
这是……契约书吗？

～～～～～～～～～～～～～～～～～～～
白黑连锁 加入契约书

＿＿＿＿＿（以下「甲」）和、白黑连锁（以下「乙」）的甲乙之间，将缔结如下契约。

一、甲方、须锻炼身心，作为战士又或是魔法师、将与之相衬之姿铭记于心。
二、甲方、须在魔王袭来之际全面护援圣战士，且同意参战魔王讨伐。
三、甲方、须在同意加入白或者黑的派系并同意乙方的团体活动和思想。

作为此契约之证，此契约将生成两份，甲乙双方署名後，各自保管一份。

战魔历九十一年四月一日

　甲　＿＿＿＿＿
　乙　白黑连锁

　～～～～～～～～～～～～～～～～～～～

真是不合情理的契约书啊。
一和二就算了，三为啥是强制的？白黑连锁的活动啊思想啊我可没听过，全被暧昧的話语糊弄过去了。换个角度，不就是叫我去死我就得去死的契约书吗。

“这还真是狠毒啊——”

波奇对着我小声地嘀咕。
莉娜也时不时看向我这边，看来也很困扰啊。
我虽然很震惊但是对象不一样。……让我吃惊的是其他的人。大家都纷纷署名，然後按顺序将契约书投入讲坛上的白黑之箱内。
那两个箱子……也就是说投进白箱就是从属白之派系吗。原来如此，性质真恶劣啊。
必须在不被发现的情况下做些什麼……

“嚯喏嚯、letter edit（书信编辑）……还有、地走魔送”

我在地面上同时发动魔法和魔术，地走魔送这一魔术、将莉娜桌子上的羊皮纸运到了我的魔法阵内。

“接着是我的……letter edit”

《letter edit》……魔道具的契约书，会根據制作者的魔力发挥相应的效力。魔力越多效力越强，也越难让人打破契约。能编辑这契约书的，便是《letter edit》这一魔法了。
这是在波奇还不是使魔的时候，我搞错了重要的文献、但文字已经刻入时想出的魔法。
只要魔力超过了契约书当时的制作者，便可以编辑契约书。能拥有同我一般魔力的人恐怕没有吧，所以这个魔法才能被活用。

文字如扭捏地跳舞般跳出、编辑完後契约书变成了这样。

～～～～～～～～～～～～～～～～～～～
白黑连锁 加入契约书

＿＿＿＿＿（以下「甲」）和、白黑连锁（以下「乙」）的甲乙之间，将缔结如下契约。

一、乙方、须锻炼身心、作为战士又或是魔法师，将与之相衬之姿铭记于心。
二、乙方、须在魔王袭来之际全面护援圣战士，且同意参战魔王讨伐。
三、乙方、须在同意加入白或者黑的派系并同意乙方的团体活动和思想。

作为此契约之证，此契约将生成两份，甲乙双方署名後，各自保管一份。

战魔历九十一年四月一日

　甲　＿＿＿＿＿
　乙　白黑连锁

　～～～～～～～～～～～～～～～～～～～

一、二、三的「甲」全部改成了「乙」。也就是说我现在署名後，这份契约书也不会对我产生任何效果。
纵使魔力比我低，只要强制力有些许效用都很麻烦。
恐怕签署了契约书的其他学生，都会变成对白黑连锁唯命是从吧。
但是，由於这个原因，撇去我不说、让莉娜遭遇不测的話……？虽然麻烦，还是做些手脚吧。

接下来，问题就是那个箱子了……该怎麼办呢？
嘛、关於这个艾琳已经叮嘱过无数遍了也没办法啦。本打算在投函时，用渗透魔法让其从讲坛下掉下去之後回收的，但是莉娜也在，行使起来会挺困难的吧。
我在契约书上签名後，把契约书投进了白的箱子。
然後莉娜……

“啊啦、莉娜投了黑的耶？真遗憾呢”

波奇也惊讶得合不拢嘴。

“…………”

“因为……想在友谊赛上和【老师】战鬥……”

……这样啊。
但是，刚刚的話、已经足够吸引全班人的注目了。

“这可不能当没听见啊！”

班级中冒出一声响声。低沉、却十分洪亮。
和莉娜一样在最前排、坐在中间的男生，正是奥尔路。

“就是说、别小看友谊赛啊！”

左手後方传来具有压迫感的声音。姓名牌上，写着《米德尔斯（Middles）》这幾个字。
作为魔法师来说体格过於庞大、粗犷的褐色肌肤的男子。

“大家，都是以友谊赛的代表为目标的，可不会输给你这样的小姑娘哟！”

右手後方的女生，是个给人《虽然是美女眼神也太冷酷了》这种印象的赤髮女生。姓名牌上写的是《伊蒂亚（idea）》这幾个字。
这之後，从属黑的人也对莉娜的話起了反应。
原来如此，代表指的是一年级生的黑的代表。友谊赛的出战人数是决定好了的。一年级生的门槛相当高。

——参加人数一人。

从二年级生好像会增加至三人，但由於时间限制的关係一年级只有一人。并且，这个代表还会被其他学年的标记的样子。

莉娜微微歪了歪头。但是这是她拼命忍耐的状态。没办法……这裡就——

Dang、地回响起沉闷的声音。我用法杖狠狠锤了下地面。
自然地把注意力全拉了过来。

“啊—啊—、就算瞄准黑的代表也别给我一个个叫唤啊杂鱼们！超过了身为《首席》的我再说吧！”

“你、你说什麼！首席不该是我——”

“因为太麻烦了我就拒绝了啊！问忒蕾丝桑就知道了！”

“没错，能使役我这个level 100的使魔的master才是最强的！渺小的弱者再怎麼叫唤都是败家之犬什麼的哟！”

最後的部分希望你好好说啊。
然而，由於波奇的跟腔，周围骚动起来，动摇的人越来越多。Level 100的使魔确实是强力一击啊。

哟西、注目对象从莉娜变成我了。

“都给我安静下来”

这时，忒蕾丝敲着手开始镇定学生们。骚动比我预想的还要快就被平定了。看来契约书已经开始发挥效用了也说不定。
忒蕾丝先把莉娜叫回了座位，接着为了和我搭話走到了讲坛前。

“阿兹利桑，请回座位。”

“好的、非常抱歉。”

饱含着各种意义的道歉。
忒蕾丝也好像察觉到了那个意思。

我回到座位後，遭受着尖锐的视线度过了一天。
最扎人的还是莉娜那由於感到抱歉而投来的视线，我勉强自己不去看那双眼。


（作者：终於能入学了。）
