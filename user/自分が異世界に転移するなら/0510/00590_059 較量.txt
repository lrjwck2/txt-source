






「土屋先生、春矢就是樱姐说过的那个人么？」
]
「啊啊、对啊。就是那个一起讨伐了哥布林指挥官的春矢」

面对权藏的提问、我一边轻轻点头一边简洁的回答过去。
说起来、权藏是没见过他呢。
现在我倒是有能战胜那时的春矢的自信了。但是、春矢自那以来也不可能没有成长。
结果那样的春矢、面对着与蓬莱先生一战之後受了不轻的伤的兽人帝也毫无办法。
即使在现在、我想的也是由我亲手来报蓬莱先生还有莫娜莉娜、莫娜丽萨姐妹的仇。但是、最優先的是保证能杀掉它。
有奥加大师在的话、无论需要怎样的手段、只要能打倒兽人帝我都会绝不吝啬奉献上我的力量。

「兽人帝这傢伙、明明只是兽人却异常的强呢。听爷爷说它是在奶奶他们出现的时候、才在这个岛上现出身影的兽人」

很有可能是杀掉转移者、一口气获得了大量经验值从而提升了力量吧。

「这扇门是通向後庭的。後庭的话、可以随意战鬥了吧。那麼、在对战者过来之前、请不用拘谨——」

带着我们走下一层、来到了中庭的奥乌卡一边挥着手、一边回到了屋子裡。
中庭比我上的高中的运动场要大上一圈、因为上面只有被修剪过的草坪、所以作为交手之地相当够用了。

「该怎麼做呢——。果然、要让奥乌卡见识下我帅气的一面吧！」

在战鬥开始前就有点兴奋过度的权藏、开始做起了伸展运动。

「我就去那边看着了」

缘野来到生长在中庭的角落的一个大树下、坐在那裡一动不动恍惚的望着天空。
自来到了奥加的村子以来就奇怪的老实起来了、但是也没有在考虑什麼阴谋的感觉、就不去管她了吧。不过虽然如此、也没準备放鬆对她的警惕就是了。
还有一些时间、就来做定期报告吧。
自从飞到岛的西端之後、就有留心每日一次的和树妖线粒体的联络。
集中意识、开始建立内心的连接、想象着通过看不见的线来向对方传达声音。

『线粒体、能听见么』

『啊、是土屋！咦、声音听起来比昨天更加清晰呢！』

虽然自来到岛的西端每天都有联络、但是每次都是有各种杂音必须努力去听才能听清的水平、不过今天却能清楚地听到对方的声音。噪音和往常比要少的多。

『因为和往常一样、不能持续长时间的对话、所以我就开始报告现状了』

说了现在来到奥加的村子、这裡对转移者很友好的事情。
还有70年前就有转移者来到这裡、其中有人和奥加生下了孩子、进驻到了这个村子裡的事情。
还有就是与他们之间的会谈达成统一、準备移居到这裡的事情。这些都拜托线粒体传达给樱他们了。

『是——是。明白啦——。不过樱反反覆覆的说着、不要乱来就是了』

『我了解了。就这麼传达过去吧。还有、也传达过去权藏也还是那麼精神、有说可能春天要来了这种话呢』

『明明还没有完全到冬天呢？』

线粒体好像对这种表达方式不擅长、理解不了的样子。
说起冬天我想起来了、虽然是直到最近才自然而然的接受了来着、这个异世界裡也有四季的存在。
由於在日本的癖性、所以最近感到冷了就以为冬天已经来了、并没有起任何疑惑。知道某一天、听到樱在那独自嘟囔到。

「说起来、这个世界也有春夏秋冬吧」

虽然在日本的话是理所当然的想法、但是外国的话貌似对四季的存在比较惊讶的样子......这是在一个叫做『住在日本的外国人的吃惊之处』的电视节目中的视角看到的。
想着这麼一说还真是、於是就问了线粒体结果对方立刻答道『有哦——』。
现在应该是还没到真正冬天的深秋吧。那麼、北之森那裡应该是结了累累硕果了吧。

「在冬天到来之前、要好好确保住居住的地方呢」

「因为根本不知道会冷到什麼程度、所以有这个村子在真是帮大忙啦」

结束了一系列伸展运动的权藏插进了我的自言自语中。

「不知道雪会是怎样呢」

「我住的地方不常下雪、所以很期待这裡下雪呢。毕竟下雪的话、更有气氛呢！」

瞅你那一脸奸笑。一定是在妄想着在下着雪的街道和奥乌卡并肩散步的样子吧。
多少有点、不对、实在是相当让人恶心。

『喂喂喂喂、其他要说的呢——！』

喔、完全忘记还在与线粒体通话中了。

『不好意思。今天就这些了、我们这边都谈妥了之後、会再次联繫你的』

『尽可能早点回来哟。大家、都很寂寞的样子呢』

我也想见大家啊。线粒体、樱、格鲁霍、还有萨乌瓦。

『啊啊、一定会回去的、在稍微忍耐下吧。那麼、再见』

『是——、拜——拜』

和线粒体结束通话的感觉就和挂掉电话的感觉一模一样呢。
而通话中的状态就完全是在用手机的感觉了。

「喂、让你们久等啦！」

屋子的门被豪爽的推开之後、出现的奥加大师和奥乌卡。
然後还有一位拿着铁制的棒子的、作为奥加来说比较纤细的男人。就这三人。

「那麼、谁要先来？」

伴随着奥加大师的提问、权藏唰的举起了手走向中庭的中央处。

「首先、就来见识下我的实力吧」

权藏摆出造型说完之後、瞟了一眼奥乌卡。

「嚯嚯、不错不错。很有幹劲嘛。那麼、里奥上吧」

「是」

简短的回復之後、拿着铁棒的奥加步入中庭。
身高不到2米吧。本来会达到肩膀位置的长髮被用一根带子给扎了起来。
他眼睛眯的细细的让人误以为他是闭上了眼睛。刚才我就在想了、在肌肉男为主的奥加中、他这样的紧绷的体格更像是足球或者棒球选手一样。
那个貌似是武器的铁棒长度幾乎和他身高差不多。虽然像枪一样在有把前端做成锋芒状、但是那个还是该成为铁制的棍子吧。

「呼、这裡就大放异彩、抓住奥乌卡的心吧」

心声都露出来了啊、权藏。
不过好像没有被对方听到的样子、无表情的走向中庭的裡奥突然被人拉住了。

「里奥、要让我看到你帅气的一面哟！」

一边说着一边把裡奥的胳膊拉到自己胸前的奥乌卡、亲了裡奥的脸颊一下。

「很难为情的吧。快闪开」

虽然里奥那麼说着但是脸上表现出的却不是这样。
看着关係和睦的两个人一边不小心發出了「唉——唉」的声音、一边不情愿的望向了权藏那边。
他长大的嘴让我担心他的下巴会不会掉下来、眼睛则是让人觉得轻轻敲一下他的后脑的话他瞪着的眼珠就会掉下来的感觉。正如如上所述站在那裡的是目瞪口呆的权藏。

「奥乌卡……那个人是、你的哥哥、么、是吧？」

已经够了、不要这样了权藏。
现实中没有没有会用胸部挤压哥哥、还与哥哥接吻的妹妹。

「嗯。里奥是我最重要的男朋友啦！」

唔哇——、还真是明亮的笑容啊。虽然这个如太阳般闪耀着光辉的笑容挺不错的。但是也相当的传达出了幸福之情。
两人为恋人这件事已经确定了。而面对这件事的权藏的反应是——

「……异世界……日本……都一样……现充爆炸吧……世界什麼的毁灭掉好了……」

抱膝坐在那裡的权藏开始诅咒着世界。

「两人非常恩爱啦——。里奥今天要是让我见识到你帅气的一面的话、晚上、我也会加油的！」

「喂、喂、在人面前能别这样么」

说真的、请饶了他吧。
消沉过度都有点变得奇怪了的权藏、就这麼抱着膝向前滚去。

「就这麼滚下去、我就能变成圆形的球了呢......」

你又不是河滩上的石头、做不到的。

「权藏正在进行什麼奇怪的举动呢。这就是日本流传的武术技巧么！」

奥加大师好像误解了什麼、不过我没有吐槽的心情了。
更加悲剧的是由於他比较强的运动能力、现在权藏正在以本来不可能的形式向前滚着、而且一直持续着速度还不慢。
这下、在战鬥之前就已经确定败北了吧。我要是能在次战挽回的话还还好、但是权藏一直不振作起来的话也很麻烦。奋力相助一下吧。
姑且是用脚先停住了滚个不停的权藏。虽然脚底正好踩到了权藏的额头处、但是权藏毫不在意、还想继续滚。

「权藏。奥加的女性好像是喜欢强大的男人呢」

脚下的力量消失了。之前已经满目无光的权藏现在也稍微有了点儿精神的样子。
有反应了麼。那麼、就只能赌在这上面了。

「那个叫做裡奥的奥加、肯定是仅次於奥加大师的强者吧。所以、在见识了那场战鬥後奥乌卡才变得喜欢上了对方的吧」

啊、不过那种事从来没有听说过就是了。
权藏推开我的脚、突然站了起来。

「怎麼说呢、无论如何、这场战鬥要是权藏输了的话、那两个人、今晚就会激烈的——」

「变成我的刀锈吧！」

瞳孔恢復了光芒的权藏全身充满鬥气、摆出了居合的架势。

「煽动过火了麼」

权藏额头上的第三只眼睁开了、从那裡出来了什麼乌黑的东西、就当做没看见吧。

「奥乌卡退下、他好像是来真的」

推开奥乌卡之後、里奥放低重心架起了铁棒。

「唔姆、两人都很有鬥志呢。那麼、禁止杀害、此外任何手段皆可。打个痛快！开始吧！」

权藏一点一点用脚缩短间隔、里奥则是驾着铁棒就这麼在权藏周围徘徊。
考虑到武器的长短裡奥比较有利。但是、要是说武器性能的话权藏有着压倒性的优势。
对方的武器是用起来稍显不足的铁棒。虽然看起来很坚固的样子、但是姑且是无法匹敌传说中的武器妖刀村雨的吧。
就算考虑到双方的实力技巧、但是凭居合的威力的话那种铁棒会被斩断吧。
比起缩短到居合的攻击距离之内、我认为用居合迎击对手的进攻、击落武器这点会更加有效。
无论怎麼来、先手的都是里奥这边了吧。
在还剩两步就进入到铁棒攻击範围之时、权藏开始动了。

「两断吧、水月！」

在居合的攻击範围外使用了水月、用压力将缠绕妖刀村雨的水斩削出去、权藏突然就使出了秘技。
太胡来了、正面吃上那招会死的！
毫无宽恕的一击已与里奥近在咫尺。

「咻！」

發出尖锐的吐息同时、里奥用铁棒的前端贯穿水形成的新月的中心部位。
就像台球中击球时的架势一般、里奥架起铁棒向前突击出螺旋的轨迹、借着回转力和突击的速度从正面将水月击碎了。
把那个两断了石化蜥蜴的水月用铁棒一击防御住了。
虽然细长的铁棒容易折断、但是这种用棒的前端攻击然後从末端输入力量是极难的技巧。里奥通过用铁棒前端的突击、防住了高威力的技能。

「果然、这种程度的、还不能打倒你啊」

「老实说真让我吃惊。没想到、能达到这种程度的威力」

虽然里奥自身无伤、但是由於刚才那一击铁棒的前端已经裂为两半、前端三十厘米左右的长度裂开了。
没能冷静观察的我才是笨蛋么。权藏正是看清了对方的实力、理解了对方能防住的基础上才發出的攻击。

「切、没能从头顶给你切成两半呢」

权藏放出了懊悔的台词......看起来、有点过高评价他了。

「你挺能幹的啊」

「你这边才是、完全不能大意的对手呢」

互相认同着对方为强者、同时都在思量着下次攻击的时机。
紧张的空气中、权藏灵活的运动步法毫无準备动作就起动了、突入到对手身边、準备在这刀的攻击距离之内放出居合。
看穿了这个动作的裡奥、在居合放出之前扔掉手中铁棒、瞬间用手按住村雨的刀把尾部、阻止了村雨的出鞘。
此时、权藏放弃居合、稍微弯下腰来、用头撞向对方下颚。
里奥将剩下的一只手挡在对方的头与自己下颚之间、尽管如此也没能完全挡住攻击、脚下稍微脱离了地面。
现在浮起的状态已经无法避开攻击了。权藏没有看漏这点。
权藏为了实行追击而向前迈出一步、用掌底叩向对方的要害。
由於防御而用掉了双手的裡奥看起来已经束手无策了、但是他踢向被扔掉的铁棒的下端、然後铁棒前端划出弧线袭向权藏突进而伸出的手。

「麻烦」

虽然是绝好的机会、但是权藏还是暂且退了回来、再次拉开了距离。

「鲜血沸腾起来了、灵魂也在燃烧着」

里奥眯成一条线的眼睛突然张开、锐利的目光直射向权藏。
两人都因为棋逢对手而内心躁动不已、虽然摆出认真的表情、但是看起来好像都有些开心的样子。

「里奥我来了」

「来吧、权藏」

如多年老友一般交换着言词、又如获得了新玩具一样的孩子般露出了无邪的笑容。
两人同时迈出——

「到此为止」

突入到两人之间的奥加大师、轻描淡写的捏住了铁棒的前端、同时也用手指按住了欲拔出来的刀身。

「什！」

「大师！」

权藏發出惊讶的声音、里奥则对插手战鬥一事露出毫无掩饰的愤怒。

「继续下去的、会出人命的吧。已经充分确认到双方的手腕了。足够了」

虽然起初是以测试实力为目的不过中途开始有点过头了、双方互视一眼、露出苦笑收起了武器。

「里奥、还是那麼出色呢。今後继续如此精进就好。权藏、让我大开眼界呢。如若你恳将力量借於我们的话、我就放心了」

里奥挺直背、深深地弯下了腰。
受到坦率的称讚的权藏挠了挠头、也轻轻地低下了头。

「实在是精彩的战鬥。这下变得更加期待下场战鬥了」

奥加大师将实现转向我这边。
为了回应这份期待、我也得好好表现一下吧 。

「本来的话、老夫想自己上场、但是那样的话差距有点太大。那麼这裡......就交给奥乌卡了！」

「恩、交给我吧、爷爷！」

挺了一下巨大的胸部、奥乌卡步入场地。
我还想谁会是对手呢、结果是奥乌卡么。

「同为刀的使用者、本还想与权藏一战呢、不过土屋看起来也会很有意思的样子！」

「还请手下留情」

从初次见面时就觉得对方不是一般人了、如此对阵起来、就已经能感到其力量的片鳞而略微战慄起来。
奥乌卡也是有着相当的实力呢。
那麼在这裡、就容我做出响应全力以赴吧。

