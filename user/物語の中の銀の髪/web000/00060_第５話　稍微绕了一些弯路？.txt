


早上，刚睁开眼就发现亚里亚睡在眼前......嗯，稍微冷静下！昨天晚上确是分开床睡的啊.
「嗯？那个，为什麼蕾桑会睡在我的床上啊？」
「这~应该是我的台词」
「唉？啊啊！还真是啊.对不起.」
亚里亚慌慌张张的从被子裡钻出去......微妙的感觉睡衣怎麼比裸体更工口啊......不好不好，身体要兴奋起来了.
「总之，先做好出發準备吧」
「好，好的！我明白了！」
叠好亚里亚已经晾乾的衣服.虽说上面的污垢已经不见，但到处都有破洞，已经不适合外出穿着，就让亚里亚穿魔导院的制服吧.我伸手抚平连衣裙上的褶皱，穿上.
「哦呀，已经準备出發了吗？」
「是的，非常感谢您.」
「又没做什麼值得让你感谢的事，这只是我的工作啊.」
「那麼再见了.亚里亚，出發了哦」
「好的！」
从【冒险之巢】离开，沿着弯曲延伸的街道出發.街道上的人比昨天多了许多，露天集市也很热闹.城镇上随处可见冒险者或是商人模样打扮的人.
「......蕾桑，我这副样子很引人注意啊」
「因为挺可爱的，有什麼不好吗？」
「......虽说是可爱，姑且在离开王国前，这是违法的啊？」<Yami：大概在王国穿着大胆是犯罪吧？记得前面的骑士桑在街上巡逻都不是穿的轻装裤衩，而是裹得饺子似的>
「即便如此，偷偷摸摸什麼的也绝不是我的作风」
「话不要说得那麼死嘛......这个衣服比想象以上的，那个下面可是發出ス—ス<拟声词再次跪拜，读作：舒服的舒.好的，大家请连读2次>的声音啊」
「莫非是第一次穿迷你短裙？」
「裙子虽然经常有穿，但迷你短裙嘛.我想会穿着的人，一只手掰着手指都能算清.」
「那麼，当做第一次的经验不是挺不错的吗！」
「啊，真的那麼想就可以了麼？」
二人一边向露天集市眺望着，一边向着国境相反方向的门前进.
「啊，有卖药水哦」
「你没有储备药水累吗？」
「生鲜的药水的话没有放进道具箱哦，变坏了的话可就不好了呢.」<Yami：一般说来ポーション还会变质的咩？！>
「蕾桑的话感觉就会像那样大意哦」
「亚里亚还真是逐一逐一的做着笔记呢」<Yami：用笔迹记录爱人的点滴！クソ！>
「那边的小姐们，是要买点药剂吗？」
似乎是听到我们的对话，露天集市的商人露出一副意外的脸.大概是觉得眼前的轻装[外表上看的话]美少女[以我为基準]<Yami：泥煤！咱咋辣么想打人呢！>二人会进行只有冒险者和商人才会买的物品的会话很奇怪吧.顺带一提，不把药剂等生鲜物品放进道具箱的理由则是，神觉得道具箱中放入生鲜之物会在不知何时腐坏.变质腐烂的那个味儿可是相当的不妙.鉴于如此现实的理由，生肉之类的就被排除在外了的样子.
「那麼，请给我50个药水」
「唉-？！」
「蕾桑，这个人没有那麼多的药水卖啦」
「唔~嘛，没那麼多的话就一个都不会买.」
「......总觉得蕾桑对金钱的认知相当的厲害呢.」
「不啊，反正也能使用回復魔法，感觉也不是一定要买」
「那个，姑且先说一句.能行使回復魔法的人还不到十个哦」
「唉？有那麼少吗？说起来，昨天晚上就说会使用魔法的人很少了，这是怎麼回事？」
「要说的话呐......」
「等，请稍等下！小姐们！」
正谈到关键的时候被露天商人的喊话打断了.因为是相当在意的话题，所以我用不高兴的语气回道
「有啥事吗？这边可是正谈着对个人来说很重要的话题呢」
「呀，刚才小姐有说会使用回復魔法呐！」
「嘛，会倒是会.」
「我的妻子生了病，连药水都没法治疗！无论如何，请您帮帮忙吧！」
「药水都没办法的事，你觉得回復魔法能够治癒吗？」
「......确实如此，但我也听说过某些药物无效的疾病，依靠魔法却治癒了的事」
「说以说还请您帮帮我！报酬多少我都出！」
「姑且......先试一下吧」
「蕾桑，这样好吗？」
「不是好不好的问题，被别人拜托到这个地步了，只有答应了吧」
「万分感激！那个，小姐们方便的话现在就动身吧」
「嗯，倒是没什麼」
收拾好露天摊，商人与我们一共三人望通往他家的街道的岔道间移动.
「......看起来是迷路了呢」<Yami：商人桑你搞毛啊，带人回家救老婆还玩lost play？>
「是的，海纳教国这样的路很少，这种状况我也是第一次碰上」
「你迷路了呢~」
「是啊，要记住这条路可是相当不容易的」<Yami：敢问您家可是住迷宫都市？>
「嘛，飞起来的话总能做到些什麼呢」
「......那只能是最终手段哦」
「不过，这最终手段可不能使用呢」
「......正是如此」
「到了哦，小姐们」
终於到达的家，与周围的家比较起来没什麼大的不同，都是用砖瓦砌成的房子.进入家裡，映入眼帘的照片给人一种欧式风格房间的感觉.在这个家的二楼，睡着一名人类的女性.
「......阿伊娜，我带会回復魔法的人回来了」<Yami：新人名，咱就音译了.反正路人人妻什麼的，广大绅士转个背就忘记了>
「回復魔法？敢问是哪位大人呢？」
「我叫蕾，然後这位是跟我一起旅行的亚里亚.」
「感谢」
眼前的女性虚弱到一眼明了的地步.眼眶下是深深的阴影，脸色清白，手似乎也是经常抽搐痉挛的样子.这样的她，什麼时候死掉都不会让人觉得奇怪.
「......好年轻呐，真的会使用吗？」
「唉，姑且算会」
「......这样啊」
暂且对眼前的女人使用了技能【辅助 搜索】
阿伊娜 ♀ 等级 15 人类 城镇居民
查看更细致的现状，我默念道
HP　２３/３００　MP　３０/３０　状态异常　疾病
疾病？【MAGIC・TAIL】裡可是没有这种异常状态呐.握住阿伊娜颤抖的双手，姑且先解除异常状态，回復她的HP【魔法　フェアリーライト】<Yami：这个技能是叫妖精之光吧，妖精之光没错吧？>阿伊娜湮没在魔法的圣光之中.<Yami：虽然不大中意圣光，这儿也只能拜托她了>
HP　３００/３００　MP３０/３０　状态异常 无
「你感觉怎麼样？」
「啊啊，身体轻盈的感觉可是时隔三年了呢.非常感谢您」
「没什麼，我只是做了力所能及的事」
说实在的，这比我想象中的还要简单就解决了.但是异常状态 疾病 ，到底是什麼呢？以後可得好好调查一下啊.
「蕾桑，治疗结束了吗？」
「嗯~治好了哦」
「已，已经治好了吗！尊敬的小姐！？」
「正是如此哦，阿依嘉.这位可是我的救命恩人，仅凭言语实在无法表达谢意」<Yami：某路痴商人——阿依嘉，咱依然音译了.话说阿依嘉，阿伊娜什麼的，雀定不是兄妹咩？！>
「真的没什麼，我只是做了力所能及的事而已啦，并不是什麼了不起的事呀.」
「蕾桑其实是什麼事都能做到的吗？」
哎呀呀，就算是我也有做不到的事哦？<Yami：表示搞不懂这种莫名其妙的问号>
「那个，姑且让我送上些什麼当报酬吧......」
「嗯，那麼......能不和别人提起这件事吗？」
「唉，就只是那样吗？」
「啊，那就足够了」
总感觉传出去会变成相当麻烦的事情.
「我明白了......是关係到很重要的事吧，我会保密的」
「唔~，有劳了」
暂且，先和亚里亚一起离开这个家吧.不知何故亚里亚却露出一脸的不可思议.
「怎麼啦？」
「啊，该怎麼说呢，有点意外啊.明明堂堂正正的治疗好了呀，我想这应该是个很好的宣传才对」
「......虽然也有这样的打算啦，但是太过自负的话，病人可是会多得压过来哦.我想，如果这成为以後行动的障碍可不太好呢.而且这样做着的时候万一【魔神】来袭就不妙了啊」
「意外的考虑得很缜密啊」
「倒不如说，之前亚里亚你是觉得我是什麼都没想的么......」
「不，我不是那个意思啦」
「妈妈可真是觉得很伤心呐~」
「你才不是我的妈妈好吧！」<Yami：这个是要闹哪样？！>
「话说，亚里亚.我能问你一个问题吗？」
「什麼呀？」
「......这裡，是哪儿啊？」
「什麼都没确定你就走出来了吗！？」
「我可是坚信着亚里亚已经记住路才跟着出来的说......」
「明明是我跟着脚步毫无迷茫的蕾桑才对......」
周围全是用砖瓦砌成的家舍，老实说从刚才开始映入眼帘的景色就没变过.
「只有用飞了呐~」
「再，再走一会儿吧！一定......应该能走出去的.」
就这样，我们在岔道上诠释着徘徊与彷徨.




