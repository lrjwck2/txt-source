【唔～……】


无所事事地望着窗外缓慢流淌得景色，继那不知第幾次打着哈欠，然後擦掉眼角冒出来的眼泪。耳边回响着的只有马蹄有节奏的踢踏声。
继那一行人拿着收到的招待券，现在正坐在前来迎接的马车裡。看起来对方好像认为继那他们一定会去，送来的信裡写着王国的马车到利亚贝鲁之前，请先做好準备。
反正，有拒绝王国邀请这种骨气的人也寥寥无几，去了肯定没错的。就这样，继那一行人再次出發，取道利亚贝鲁，坐马车前往国王等待的王都。

在这个伊库利亚大陆，有三个国家。每个国家都有非常能代表本国的特徵，首都名也很通俗易懂。
首先，关於继那一行人作为基地的利亚贝鲁所属的尤斯迪利亚王国。这裡施行以国王为中心的王国制度，王国坐落于伊库利亚大陆的西南方。首都是「王都 尤紫欧华」。作为这个城市的特徵来说的话，是兽人族、人族和其他种族（妖精族等）大约以5：3:2的比例生活在一起。各种族之间的关係相对良好，至今为止尚未引起太大的争执。
但是，有较为崇尚实力主义的倾向，根據力量（字面意思 即武力）来分封贵族爵位。

其次，关於梅菲斯特巴尔帝国。不管怎麼说，只要一提到这个国家，首先被人提及的就是以魔族为中心。首都是「帝都 莱勒科」。具体构成比例，魔族与其他种族（包含人族）的比例为8:2，梅菲斯特巴尔帝国就是这样一个呈现极端态势的国家。另外，由於魔族是擅长魔法的种族，创立许多魔法学校也作为这个国家的特徵之一常被提及。主张个人主义（走自己的路的意思），由於对国王陛下的忠诚，所以没有内部分裂的情况。
此外，虽然梅菲斯特巴尔帝国是以魔族为中心，对其他种族却也并不存在排外现象。

最後，就是继那出生（重生）的雷曼迪利亚神圣国。这个国家因弘扬人族至上主义而非常有名。首都是「圣都 里巴伊尔」，特徵，此帝国排他思想已经历来已久，历史上的权力之争也已司空见惯。种族构成比例，人族与其他种族比例为8:2，人族以外的种族无人担任国家要职，人族与其他种族之间有着巨大的鸿沟。作为目前在大陆广为流传的魔法体系理论的创造之国而闻名，对魔法研究的投入也比较大。
这个国家有一个名为七煌教会的宗教，此宗教是雷曼迪利亚神圣国的根基，现行的魔法体系理论正是发源於此教会。

【这样怎样？】
【嗷っ！？　这样子啊！　……那麼，我就这样！】
【这样的话、我就这裡】
【啊哈！　竟然还有留有这一手！？】
继那回过神往马车裡一看，马车中，两组妹子分别互相面对面，将一块小板放在面前。一边是叽叽喳喳喧嚣不停正玩得兴起的奇莉娅和索拉。她们在8*8的棋盘裡放上棋子（译者注：黑白棋），玩得噼噼啪啪你来我往互不相让。
另外，是一边默默地冥思苦想一边移动着黑白小马驹的莉莉娅和希尔薇。马车裡，看到这样一对截然相反的气氛互相掺杂着，以及各自享受着遊戏乐趣的她们，继那也不由得展颜了。继那的内心，真正得被触动了---在异世界生活的人，现在如此享受地球上诞生的遊戏。
（就算世界改变了，时代背景不同了，但是人的根性却是相同的......就是这麼一回事吧？）

玩遊戏的人们并不知道继那现在的所思所想，眼前和气蔼蔼的声音交错着，仍旧是一片平和的景象。
莉莉娅他们感兴趣得是黑白棋和国际象棋。这些都是继那制作的。当初继那只是为了提升自己的木工技能，利用空闲时间作着玩儿的。（只是单纯地作木工，好无聊啊）这样想着，（那个时候做得）黑白棋的话，游戏规则也相对比较简单的，到时候可以邀请奇莉娅和索拉，作为饭後娱乐而制作的。
顺便说一句，棋子的颜色只是採用了简单的处理手法---灼烧削好得木块。

【吼，说起黑白棋，这个……】
正在和索拉玩得开心的时候，最先感兴趣的是莉莉娅。之前一直在卧室读书的莉莉娅
，突然，在一旁的莉莉亚發出了颇有兴趣的声音。在听了遊戏的说明以後，【原来如此，看上去很简单，却意外的有深度呢】，莉莉娅这样自言自语着嗨了起来。看到这，继那不由得苦笑了起来。
後来，莉莉娅又锲而不舍的追问了【还有其他的吗】，於是，继那又制作了其他的几种遊戏。从黑白棋开始，陆续做了国际象棋，扑克，将棋还有围棋，甚至还做了双六。
多亏此，虽然继那的木工技术如当初的目的一样提高了不少。但是，这之後，继那沦落到经常被她们要求陪同玩遊戏的境地（可能是经常输??），不知这是否也算是自食其果了，烦恼ING。
後来通过莉莉娅得知，在这个异世界，这样的智力遊戏很稀缺，一般情况下要不是读书就是在外边玩。

可能是废话（但我觉得值得一提~2333)，莉莉娅和希尔薇喜欢国际象棋和将棋，索拉喜欢黑白棋和扑克。奇莉娅是全能型的，但是双六比较强。
（哦呀~真是一派和平安稳的景象）
真不是一般的悠闲啊，完全无法想象他们是最近刚参与了【奇拉美领主】这个灾难事件的当事人，这样想着的继那露出招牌自嘲式的笑容。
再度把视线转向窗外的继那，在女孩子们的嬉笑声中不知不觉地回忆起过往来。

◆◇◆◇◆◇◆◇◆◇◆◇◆◇
从利亚贝鲁的街道出發後第三天的午後。
一路上，继那一行人乘坐的马车并没有被發生【遇到盗贼】之类的事情（毋庸置疑没有遇上盗贼自然是最好的），然後如约到达了高恩拉利亚街。这条街（从利亚贝鲁来看的话）是离王都最近的一条街。也许是离王都比较近的地理条件的原因，这裡的商人比利亚贝鲁多得多。连进这条街都要花半刻钟（30分钟），也只有在这条街才会这样吧。
【ORZ～。终於到了……】
【真是的，明明只是进个城，没想到要花这麼时间。】
奇莉娅噘着嘴抱怨道。
【正是这样才说明了这裡确实是很热闹繁华呢】
莉莉娅说道，果然是大人的处世之道
被驾车的男子催了一声【到了】，之後，一行人就下了马车进入了一家旅馆。这正是高恩拉利亚首屈一指的旅店-----今宵月。看到住宿费的瞬间，普通的冒险者都会速速离去的那种高级旅店。
内部装修自不必说，各个房间的气氛也让人觉得很安稳，每个人仔细工作的情景也一目了然。
【哇～……。我还是第一次住这麼好的地方呢……】
索拉微微摇动着双耳开心地说道，【真的有这种地方…】奇莉娅也被旅馆的风格所震惊。
【会有什麼样的料理呢，好期待啊～】
【我很喜欢这裡安定的氛围】
另一边，希尔薇和莉莉娅还算保持了平时平静的样子。与她们的反应截然相反，继那露出了一副让人看上去就不禁想叹气得忧郁的表情。

【怎麼了？　能住在这种高级旅馆的机会可不多吧？】
莉莉娅一边坏心眼地暗笑着，一边向继那问道。莉莉娅这个人，就喜欢明知故问，真是腹黑.....继那一边这样想着一边回答道，【想到之後的事情，觉得有点担心却又不知道该做什麼……】。

【客人，您的同伴到了】

到旅馆後，正在想着【这之後该怎麼办】的继那，不经意间，被突然出现在眼前朝自己说话的服务员吓了一跳。
【同伴？！……。这是怎麼回事？】希尔薇不由得思索了起来。
【嘛.....我也不清楚】 奇莉娅的心头也浮现出了疑问。
【继那～你听说什麼了吗？」
【没，什麼都没听说......那封信上并没有关於同伴什麼的事情啊】 继那一边回想信的内容一边侧头回答询问的索拉。
【管他呢，见了不就知道了吗？】，莉莉娅这样说道。
【恩，也只有这样了。】，继那回答莉莉娅说。
於是继那在服务员的引导下走向 旅店的玄关。
【――你就是继那·佐伯吧。……速速来与我一较高下！】
继那一出房间，大家就听到这样一声大叫。