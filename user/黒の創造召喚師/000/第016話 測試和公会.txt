

「继那，好好拿着呢？」
「拿着了。……真是的还是那麼爱操心呢」
从门的对面听见西尔维不安的声音，继那苦笑着回答。结束了每日的空挥，返回房间的他比平时都细心地準备着。
（终於，从今天……开始吗）
像是为了消除夹杂着的些许不安一样，阳光透过窗户照射着继那。
「说起来，在这裡住了快2年啊……」
完全收拾整齐的自己的房间。尽管与在那所房子裡度过的时间差不多一样，但在这裡度过的每天反而让人怀念，感觉像是發出耀眼的光辉一般。
「继那~。师傅叫你哦」
听到楼下传来西尔维的催促声，回到现实的继那。返还道具箱，结束了打扮的最後检查。

「——我出發了」

继那小声嘟哝着，谁也没有回答。只是，就像是在这裡度过年幼的自己对现在的自己说「一路平安」一样。

◆◇◆◇◆◇◆◇◆◇◆◇◆◇

「继那也快要7岁了吗……」
昨晚，饭後休息时坐在继那正对面的莉莉亚感慨着。
「是呢」
「唔呣。……正是时候吗？」
对窃笑的莉莉亚，觉得「又在考虑什麼麻烦的事情……」的继那，只是小口喝着茶什麼都没说。
至今为止的来往，对坏笑的莉莉亚说什麼都是徒劳的，对於这点继那深切地理解到而感到腻烦。
「正好。去公会登录成为冒险者，顺便在那一带测试」
那样嘟哝着的莉莉亚绝对不是在开玩笑。在这个世界，认为到了7岁就能自己做（一次）决定，在公会进行冒险者登录也能被承认。但是，那个年龄若是接受了危险度高的讨伐委托将不会被承认（也有被认可的例外）。那个年龄就算进行了冒险者登录，一般最多也只能处理在城市附近的採集委托和城市居民寄来的委托（杂务委托）。因此，实际上到了12～15岁才进行冒险者登录。
若是想作为例外得到公会的承认。需要由公会一方测试实力，若被公会承认「讨伐委托也能充分地处理」的话即使是7岁也能和其他冒险者一样接受同样的委托。
（测试……呐。只希望是正经的东西……）
代替叹气继那再喝了一小口茶润润喉。不管是怎样的内容，继那都没有「拒绝」的选项。
顺带一提，莉莉亚露上一次露出这样的坏笑是「接近极限的实战经验才能得到巨大的经验！」，当时将继那丢进卡里基亚大森林最深处的迷宫将近两周的时间。
一开始想办法平安无事地（虽说也有危险的经验）解决了，但是从中盘开始难易度飞升，不禁想「我要死了吗？」经歷了好多次走马灯的感觉。
但不愧是得到了这种程度的经验，等级像泡沫经济时的股票一样连日涨个不停。
另外，提升自己的等级到25级後，在包括利尔在内的从者们的育成上花了时间。继那自身也好好强化了，不过考虑到今後的事，判断要预先把力量提高到某种程度才行。
在那两周的期限内竭尽全力踏破了最下层中被称作「上层阶段」的区域。而且，就像莉莉亚说的那样经验狂飙。（只是，莉莉亚做过头了，结果被西尔维狠狠地说教，当晚被施与没有晚饭这种形似拷问的惩罚）
虽然落得狼狈不堪，但没有莉莉亚的话就没有现在的继那。关於这一点，继那（真的）挺感谢她的。
「然後呢？师傅，姑且听一听……那个『测试』是？」
因为知道至今为止的莉莉亚（的暴走次数），西尔维迅速立起防线。
「嗯？不是什麼大不了的事。在公会登录冒险者，自己一个人生活一个月」
「一个人生活一个月……吗」
「没错。总是依靠我们也不行呐。7岁以後就能在公会登录成为冒险者。嘛，普通来说考虑到安全性，就算是提前登录，接受委托也会有很多的限制。只是，这个对继那不适用。我和西尔维觉得你『没问题』，实际上这幾年你独自一人在这个森林狩猎也是事实。实力没问题。加上在公会登录也能成为身分证明。……对一直在这个森林中生活的继那来说将会是第一次的城市生活，全部不一个人做完不行。差不多也是能自立的时候了吧？」
「虽、虽然是这样没错……」
这个世界街上有公会存在。公会的作用就是遊戏和动画片中熟知的「委托中介」。预先在公会「委托」市民和城市的「问题」，作为中介帮助冒险者。
冒险者达成委托得到每天的生活粮食，市民和城市的「问题」也能得到解决。公会在这个依库利亚大陆的三大国家的主要城市都有设立，有很大的权力。
西尔维像是哪裡不满的样子，莉莉亚所说的都是正确的所以也不能摆出强烈的态度。看到这样莉莉亚呆然地「呀勒呀勒……」發出了叹息。
「你要怎麼做、继那」
「我……」
继那瞥见在一旁眼角闪泪的西尔维。

「——想去城裡」
即答。

◆◇◆◇◆◇◆◇◆◇◆◇◆◇

「那麼，我出發了！」
「啊，去吧。离这裡最近的是尤斯迪利亚王国的城市、『真实之铃』。因为是十分大的城市，所以大部分的事情都没问题的吧。除了人族以外，也有兽人族以及像我和西尔维一样的妖精族在那个国家。不会受到什麼差别对待」
「了解。那麼，在那裡待1个月、对吧」
浮现笑颜的继那和莉莉亚。与西尔维阴沉沉的蒙上了阴影的脸形成了鲜明的反差。
「那个，不能好好地送别吗」
事到如今，莉莉亚用不容分说的语调劝告着「但是……但是~~」这样表现出懊悔表情的西尔维。

「继那要是出發了，以後不就不能再……『GIU！』地抱紧了不是吗！」

「「不要这麼做最好」」
对把继那当作弟弟看待的西尔维来说，这次是『迟早要做好心理準备的事』她是再明白不过的，可一旦这样的现实来临时却是想象以上的艰难。
「真是的，西尔维太过度保护了哟~」
「与其说是『过度保护』不如说是没完没了的占有欲呢」
（……西尔维果然——是弟控呢、唔呣）
继那回想起西尔维至今为止的所作所为——浑身鸡皮疙瘩颤抖不止。
「吼啦，赶紧出發吧。西尔维（这傢伙）差不多要变得不可收拾了」
「是是」
对在旁边大吵大闹的西尔维而束手无策的莉莉亚。在内心深处「请节哀顺变……」地嘟哝着的继那向城市迈出了脚步。




继那在森林裡咯吱咯吱地走着，就在差不多要离开森林时。

「「库啊啊啊啊啊！」」

继那遇到了两头佩刀熊为了争夺地盘扭打在一起的场面。对佩刀熊来说，继那是重要决鬥中的闯入者。
气血上冲的两头佩刀熊与（看起来很弱的）一言不发的继那不同。
「「库啊啊啊啊啊啊！」」
协力打倒眼前的敌人继那。仅仅是为了那样。
「……啊~、果然」
继那把手贴到额头上正打算要说「（あいたたたあぁぁぁ）痛痛痛痛痛……」。但是，马上从头换至后腰拔出别在后腰的两把短剑，毫不犹豫地迈步向前。
得到莉莉亚直传魔闘技锻炼後的继那本领更上一层楼，他犹如子弹一般突然加速。
「呜哩呀啊啊啊啊！」
瞬间钻入两头佩刀熊之间的继那挥舞两手上的短剑，开始对双方牵制和攻击。继那接近退怯的一头佩刀熊，「嘿！」的吆喝着放出踢击。
继那的踢击不偏不倚粉碎了敌人的头骨，当场死亡。
「还剩下一只！」
因为拉开了距离，继那踢地跃进。当然，魔闘技仍在使用。
「库啊啊啊啊啊啊！」
提升鬥气冲向佩刀熊。回避其挥出的拳头，转眼一瞬继那便钻入了佩刀熊怀里。
「呜啦呜啦乌啦啦啦啦啦啦啦！」
使出了猛烈的刺突。

——二刀短剑术Lv.2的剑技、「徒花」。

猛地刺出最後一击、佩刀熊保持着身子弯曲的姿势被击飞，已经变成不是可以一笑了之的光景。
「哇啊~真是的，溅出来的血真是夸张呢……」
因为以佩刀熊那样的体格为目标进行了幾十次的突刺，这也是理所当然的结果。
「嘛从森林出去以後把身上的衣服也换了吧」
继那唤出道具箱，收纳起刚刚杀死的佩刀熊，一边嘟哝着「这个素材的卖价大约是多少呢……」一边走出森林。

◆◇◆◇◆◇◆◇◆◇◆◇◆◇
译者后话：
一边翻译一边複习实在集中不能，这下可算能集中注意力肛一波考试了。
六月中旬考试结束再把剩下的17—20话翻完。
洗洗睡吧......
